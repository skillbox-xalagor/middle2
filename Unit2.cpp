﻿#include <iostream>

struct InvSize
{
	size_t SizeX;
	size_t SizeY;
};

class Item
{
public:
	Item(
		int Id = 1 + (std::rand() % (INT_MAX)),
		int Count = 1 + (std::rand() % 99))
	{
		this->Id = Id;
		this->Count = Count;
	}

	Item(const Item& Other)
	{
		Id = Other.Id;
		Count = Other.Count;
	}

	friend std::ostream& operator<<(std::ostream& out, const Item& obj);
private:
	int Id;
	int Count;
};

class GodObject
{
private:
	Item*** Inventory;
	InvSize InventorySize;
	std::string* Name;
	GodObject* FatherGodObject;
public:
	GodObject(InvSize InventorySize = { 3, 3 }, std::string* Name = new std::string("Name"), GodObject* FatherGodObject = nullptr)
	{
		this->InventorySize = InventorySize;
		this->Name = Name;
		this->FatherGodObject = FatherGodObject;
		Inventory = new Item** [InventorySize.SizeY];
		for (int i = 0; i < InventorySize.SizeY; i++)
		{
			Inventory[i] = new Item* [InventorySize.SizeX];
			for (int j = 0; j < InventorySize.SizeY; j++)
				Inventory[i][j] = new Item();
		}
	}

	GodObject(const GodObject& Other)
	{
		InventorySize = Other.InventorySize;
		Name = Other.Name;
		FatherGodObject = Other.FatherGodObject;
		Inventory = new Item** [Other.InventorySize.SizeY];
		for (int i = 0; i < Other.InventorySize.SizeY; i++)
		{
			Inventory[i] = new Item* [Other.InventorySize.SizeX];
			for (int j = 0; j < Other.InventorySize.SizeY; j++)
				Inventory[i][j] = new Item(*(Other.Inventory[i][j]));
		}
	}

	friend std::ostream& operator<<(std::ostream& out, const GodObject& obj);
};

int main()
{
	GodObject* YShaarj = new GodObject();
	GodObject* CThun = new GodObject({1, 1}, new std::string("C'Thun"), YShaarj);
	GodObject* Player = new GodObject({32, 32}, new std::string("Player"), nullptr);
	GodObject* Player2(Player);

	std::cout << *YShaarj;
	std::cout << *CThun;
	std::cout << *Player;
	std::cout << *Player2;
}

std::ostream& operator<<(std::ostream& out, const Item& obj)
{
	out << '\t' << "ID: " << obj.Id << '\t' << "Count: " << obj.Count << '\n';
	return out;
}

std::ostream& operator<<(std::ostream& out, const GodObject& obj)
{
	out << "Name:" << *(obj.Name) 
		<< "\tFather:" << (obj.FatherGodObject ? *(obj.FatherGodObject->Name) : *(new std::string("none")))
		<< "\tInventorySize: (" << obj.InventorySize.SizeX << ", " << obj.InventorySize.SizeY << ")"
		<< "Inventory:\n";
	for (size_t i = 0; i < obj.InventorySize.SizeY; i++)
	{
		for (size_t j = 0; j < obj.InventorySize.SizeX; j++)
		{
			if (&(obj.Inventory[i][j]))
			{
				out << "(" << j << ", " << i << "): " << *(obj.Inventory[i][j]) << '\n';
			}
		}
	}
	return out;
}
